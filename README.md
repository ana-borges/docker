# Docker images

This repository contains the Dockerfiles of some images used in my projects.

## Installing and using Docker

Follow the instructions in [DockerHub][docker-docs]. Note that if you are using
Linux, you will need to [login through the CLI][login].

## Building and pushing a Dockerfile

Assuming the current directory has a valid Dockerfile.

Build:
```
docker build -t test .
```

Test if it works. The following command will run the image:
```
docker run -ti --rm --name mytest test
```

If you want the test image to have access to your current directory you can run
this command:
```
docker run -ti --rm --name tmp --mount source=(pwd),destination=/work,type=bind alpine:3.14.0
```

Note that `type=bind` mounts the directory and any changes done in the container
will persist in the host machine.

Assuming you tagged your image with the tag `test` as shown above, after testing
your image you can change the tag to the definitive one:
```
docker tag test anaagvb/alectryon:coq8.15-mathcomp1.13
```

After that you are ready to upload the image to [Docker hub][docker-hub]:
```
docker push anaagvb/alectryon:coq8.15-mathcomp1.13
```

If you run into `no such host` problems, [try][solution] Editing the DNS
nameserver in `/etc/resolv.conf`:
```
#nameserver x.x.x.x
nameserver 8.8.8.8  
```

Be sure to change `anaagvb` to your Docker Hub user name.

For the future, it might be interesting to check out the [Docker
Keeper][docker-keeper].

[docker-docs]: https://docs.docker.com/docker-hub/
[login]: https://docs.docker.com/engine/reference/commandline/login/
[docker-hub]: https://hub.docker.com/
[docker-keeper]: https://gitlab.com/erikmd/docker-keeper
[solution]: https://stackoverflow.com/a/45011909/2305458
